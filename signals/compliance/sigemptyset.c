// The sigemptyset() function initializes the signal set pointed to by set, such that all signals defined in POSIX.1-2017 are excluded.

/* CBC3BS17
   This example initializes a set of signals to an empty set.
 */
#define _OPEN_SYS
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

main() {
  struct sigaction sact;
  
  if (sigemptyset(&sact.sa_mask)!=0){
    perror("sigemptyset failed");
    exit(1);
  }
  puts("sigempyset passed");
}
