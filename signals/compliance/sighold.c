//The sighold() function shall add sig to the signal mask of the calling process.
#define _OPEN_SYS
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
// we're not doing this yet
main() {
  struct sigaction sact;
  
  if (sigemptyset(&sact.sa_mask)!=0){
    perror("sigemptyset failed");
    exit(1);
  }

  for (int i = 1; i<32; i++){
    // printf("in a for loop\n");
    if (sighold(i)!=0){
      printf("sighold failed for %d", i);
      exit(1);
    } else {
      printf("sighold passed for %d\n", i);
    }
  }
  puts("sigempyset passed");
}