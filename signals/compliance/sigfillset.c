// The sigfillset() function shall initialize the signal set pointed to by set, such that all signals defined in this volume of POSIX.1-2017 are included.

/* CBC3BS18
   This example initializes a set of signals to a complete set.
 */
#define _OPEN_SYS
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

main() {
  sigset_t sigset;

  if (sigfillset(&sigset)!=0){
    perror("sigfillset failed");
    exit(1);
  }
}