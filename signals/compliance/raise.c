// The raise() function shall send the signal sig to the executing [CX] [Option Start]  thread or process. [Option End] If a signal handler is called, the raise() function shall not return until after the signal handler does.

// [CX] [Option Start] The effect of the raise() function shall be equivalent to calling:

// pthread_kill(pthread_self(), sig);

/* CBC3BR01
   This example establishes a signal handler called sig_hand for the signal SIGUSR1.
   The signal handler is called whenever the SIGUSR1 signal is raised and
   will ignore the first nine occurrences of the signal.
   On the tenth raised signal, it exits the program with an error code of 10.
   Note that the signal handler must be reestablished each time it is called.
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sig_hand(int);   /* declaration of sig_hand() as a function */
void sig_hand2(int);
void sig_hand3(int);
int i;

int main(void)
{

   for (int i = 0; i< 32; i++){
      //   printf("i is %d\n", i);
      //   kill_test(actions[i]);
    }
   signal(1, sig_hand);  /* set up handler for SIGUSR1 */
   for (i=0; i<9; ++i)
      raise(i);             /* signal SIGUSR1 is raised */
   signal(10, sig_hand2);
   for (i=10; i<19; ++i){
      raise(i);
   }
   signal(20, sig_hand3);
   for (i=20; i<32; ++i){
      raise(i);
   }
   exit(0);
}                            /* sig_hand() is called */

void sig_hand(int)

{
   static int count = 1;          /* initialized only once */

   count++;
   printf("%d \n", count);
   if (count == 9) { /* ignore the first 9 occurrences of this signal */
      printf("reached 9th signal\n");
      // exit(0);
      return;
   }
   else
      // printf("sig_hand is %d\n", sig_hand);
      signal(count, sig_hand);  /* set up the handler again */
}

void sig_hand2(int)

{
   static int count = 10;          /* initialized only once */

   count++;
   printf("%d \n", count);
   if (count == 19) { /* ignore the first 9 occurrences of this signal */
      printf("reached 19th signal\n");
      // exit(0);
      return;
   }
   else
      printf("count is %d\n", count);
      signal(count, sig_hand2);  /* set up the handler again */
}

void sig_hand3(int)

{
   static int count = 20;          /* initialized only once */

   count++;
   printf("%d \n", count);
   if (count == 32) { /* ignore the first 9 occurrences of this signal */
      printf("reached 32nd signal\n");
      // exit(0);
      return;
   }
   else
      printf("count3 is %d\n", count);
      signal(count, sig_hand3);  /* set up the handler again */
}