//The sigignore() function shall set the disposition of sig to SIG_IGN.

#define _OPEN_SYS
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

main() {
  struct sigaction sact;
  
  if (sigemptyset(&sact.sa_mask)!=0){
    perror("sigemptyset failed");
    exit(1);
  }

  for (int i = 1; i<32; i++){
    // printf("in a for loop\n");
    if (i == 9 || i==19){
        continue;
    }
    if (sigignore(i)!=0){
      printf("sigignore failed for %d", i);
      exit(1);
    } else {
      printf("sigignore passed for %d\n", i);
    }
  }
  puts("sigempyset passed");
}