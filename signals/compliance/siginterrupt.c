

    // The siginterrupt() function shall change the restart behavior when a function is interrupted by the specified signal. The function siginterrupt(sig, flag) has an effect as if implemented as:

    // int siginterrupt(int sig, int flag) {
    //     int ret;
    //     struct sigaction act;


    //     (void) sigaction(sig, NULL, &act);
    //     if (flag)
    //         act.sa_flags &= ˜SA_RESTART;
    //     else
    //         act.sa_flags |= SA_RESTART;
    //     ret = sigaction(sig, &act, NULL);
    //     return ret;
    // }


#define _OPEN_SYS
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

main() {
  struct sigaction sact;
  
  if (sigemptyset(&sact.sa_mask)!=0){
    perror("sigemptyset failed");
    exit(1);
  }

  for (int i = 1; i<32; i++){
    // printf("in a for loop\n");
    if (i == 9 || i==19){
        continue;
    }
    if (siginterrupt(i, 1)!=0){
      printf("siginterrupt failed for %d", i);
      exit(1);
    } else {
      printf("siginterrupt passed for %d\n", i);
    }
  }
  puts("sigempyset passed");
}
