#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h> 
#include <unistd.h> 
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>


struct signalAction {
    int signal;
    char action;
    char message[50];
};

int main (){
    struct signalAction actions[32];
    for (int i = 0; i < 32; i++){
        actions[i].signal = i;
        // actions[i].message = "";
    }
    actions[0].action = 'I'; // unused
    actions[SIGHUP].action = 'T';
    actions[SIGINT].action = 'T';
    actions[SIGQUIT].action = 'A';
    actions[SIGILL].action = 'A';
    actions[SIGTRAP].action = 'A';
    actions[SIGABRT].action = 'A';
    actions[SIGBUS].action = 'A';
    actions[SIGFPE].action = 'A';
    actions[SIGKILL].action = 'T';
    actions[SIGUSR1].action = 'T';
    actions[SIGSEGV].action = 'A';
    actions[SIGUSR2].action = 'T';
    actions[SIGPIPE].action = 'T';
    actions[SIGALRM].action = 'T';
    actions[SIGTERM].action = 'S';
    actions[SIGSTKFLT].action = 'T';
    actions[SIGCHLD].action = 'I';
    actions[SIGCONT].action = 'C';
    actions[SIGSTOP].action = 'S';
    actions[SIGTSTP].action = 'S';
    actions[SIGTTIN].action = 'S';
    actions[SIGTTOU].action = 'S';
    actions[SIGURG].action = 'I';
    actions[SIGXCPU].action = 'A';
    actions[SIGXFSZ].action = 'A';
    actions[SIGVTALRM].action = 'T';
    actions[SIGPROF].action = 'T';
    actions[SIGWINCH].action = 'I';
    actions[SIGIO].action = 'I';
    actions[SIGPWR].action = 'I';
    actions[SIGSYS].action = 'A';


    for (int i = 0; i< 32; i++){
        printf("i is %d\n", i);
        kill_test(actions[i]);
    }
    kill(25581, 0);
    int err = errno;
    if (err == ESRCH){
        printf("there is no process with that number\n");
    }
    // printf("Value of errno: %d\n", errno); // should be 3
    return 0;
    
}

// If pid is greater than 0, sig shall be sent to the process whose process ID is equal to pid.
int kill_test(struct signalAction signalTest){
    switch (signalTest.action){
        case 'T':
            printf("case T\n");
            pid_t p = fork();
            if (p > 0){
                kill(p, signalTest.signal);
                int status;
                if (waitpid(p, &status, 0) == -1){
                    perror("waitpid failed");
                    return EXIT_FAILURE;
                }

                if ( WIFSIGNALED(status) ) {
                    const int es = WTERMSIG(status);
                    printf("exit status was %d, %d\n", es, p);
                }
            }
            if (p == 0){
                sleep(1);
            }
            if(p<0){ 
                perror("fork fail"); 
                exit(1); 
            }
        break;
        case 'A':
            printf("case A\n");
            p = fork();
            if (p > 0){
                kill(p, signalTest.signal);
                int status;
                if (waitpid(p, &status, 0) == -1){
                    perror("waitpid failed");
                    return EXIT_FAILURE;
                }

                if ( WIFSIGNALED(status) ) {
                    const int es = WTERMSIG(status);
                    printf("exit status was %d, %d\n", es, p);
                }
            }
            if (p == 0){
                sleep(1);
            }
            if(p<0){ 
                perror("fork fail"); 
                exit(1); 
            }
        break;
        case 'C':
            printf("case C\n");
            p = fork();
            if (p > 0){
                kill(p, 15);
                sleep(1);
                kill(p, signalTest.signal);
                sleep(1);
                kill(p, 2);
                int status;
                if (waitpid(p, &status, 0) == -1){
                    perror("waitpid failed");
                    return EXIT_FAILURE;
                }

                if ( WIFSIGNALED(status) ) {
                    const int es = WTERMSIG(status);
                    printf("exit status was %d, %d\n", es, p);
                }
            }
            if (p == 0){
                sleep(1);
            }
            if(p<0){ 
                perror("fork fail"); 
                exit(1); 
            }
        break;
        case 'S':
            printf("case S\n");
            p = fork();
            if (p > 0){
                kill(p, signalTest.signal);
                sleep(1);
                kill(p, 18);
                sleep(1);
                kill(p, 2);
                int status;
                if (waitpid(p, &status, 0) == -1){
                    perror("waitpid failed");
                    return EXIT_FAILURE;
                }

                if ( WIFSIGNALED(status) ) {
                    const int es = WTERMSIG(status);
                    printf("exit status was %d, %d\n", es, p);
                }
            }
            if (p == 0){
                sleep(1);
            }
            if(p<0){ 
                perror("fork fail"); 
                exit(1); 
            }
        break;
        case 'I':
            printf("case I\n");
            p = fork();
            if (p > 0){
                kill(p, signalTest.signal);
                sleep(1);
                kill(p, 2);
                int status;
                if (waitpid(p, &status, 0) == -1){
                    perror("waitpid failed");
                    return EXIT_FAILURE;
                }

                if ( WIFSIGNALED(status) ) {
                    const int es = WTERMSIG(status);
                    printf("exit status was %d, %d\n", es, p);
                }
            }
            if (p == 0){
                sleep(1);
            }
            if(p<0){ 
                perror("fork fail"); 
                exit(1); 
            }
    }
     
}

//For a process to have permission to send a signal to a process designated by pid, unless the sending process has appropriate privileges, the real or effective user ID of the sending process shall match the real or saved set-user-ID of the receiving process.
int kill_permission_test() {

}

//If pid is 0, sig shall be sent to all processes (excluding an unspecified set of system processes) whose process group ID is equal to the process group ID of the sender, and for which the process has permission to send a signal.
int kill_group_test() {

}

//If pid is -1, sig shall be sent to all processes (excluding an unspecified set of system processes) for which the process has permission to send that signal.
int kill_all_test() {

}

// If pid is negative, but not -1, sig shall be sent to all processes (excluding an unspecified set of system processes) whose process group ID is equal to the absolute value of pid, and for which the process has permission to send a signal.
int kill_negative_test() {

}

// If the value of pid causes sig to be generated for the sending process, and if sig is not blocked for the calling thread and if no other thread has sig unblocked or is waiting in a sigwait() function for sig, either sig or at least one pending unblocked signal shall be delivered to the sending thread before kill() returns.
// I don't know what this means

